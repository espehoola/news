<?php

namespace common\models\query;


use creocoder\nestedsets\NestedSetsQueryBehavior;

/**
 * Class TopicQuery
 * @package common\models\query
 */
class TopicQuery extends BaseQuery
{
    public function behaviors() {
        return [
            NestedSetsQueryBehavior::class,
        ];
    }
}