<?php

namespace common\models\query;


use common\models\BaseModel;

/**
 * This is the ActiveQuery class for [[\app\models\BaseModel]].
 *
 * @see \common\models\Product
 */
class BaseQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere(['status' => BaseModel::STATUS_ACTIVE]);
    }
}
