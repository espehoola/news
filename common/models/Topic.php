<?php

namespace common\models;

use common\models\query\TopicQuery;
use creocoder\nestedsets\NestedSetsBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "topic".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property int $status
 * @property int $created_at
 * @property int $created_by
 * @property int $updated_at
 * @property int $updated_by
 * @property int $depth
 * @property int $lft
 * @property int $rgt
 *
 * @property News[] $news
 * @mixin NestedSetsBehavior
 */
class Topic extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'topic';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            BlameableBehavior::class,
            'tree' => [
                'class' => NestedSetsBehavior::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'status', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'title' => Yii::t('app', 'Title'),
            'depth' => Yii::t('app', 'Depth'),
            'lft' => Yii::t('app', 'Lft'),
            'rgt' => Yii::t('app', 'Rgt'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasMany(News::class, ['topic_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getParent()
    {
        $topicsArray = Topic::find()->select(['id', 'title'])->active()->asArray()->all();

        return ArrayHelper::map($topicsArray, 'id', 'title');
    }

    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
        ];
    }

    /**
     * @return query\TopicQuery
     */
    public static function find()
    {
        return new TopicQuery(get_called_class());
    }

    /**
     * @return bool|false|int
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function saveRecord()
    {
        if (empty($this->parent_id)) {
            if ($this->isNewRecord) {

                return $this->makeRoot();
            } else {

                return $this->update();
            }
        } else {
            $parent = Topic::findOne($this->parent_id);

            return $this->appendTo($parent);
        }
    }
}
