<?php

namespace common\models;


use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class BaseModel
 * @package common\models
 */
abstract class BaseModel extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_NOACTIVE = 0;

    public static function getTitleArray()
    {
        $idTitleArray = self::find()->select(['id', 'title'])->active()->asArray()->all();

        return ArrayHelper::map($idTitleArray, 'id', 'title');
    }

    /**
     * @return array
     */
    public static function getStatusArray()
    {
        return [
            self::STATUS_ACTIVE => 'Active',
            self::STATUS_NOACTIVE => 'Noactive'
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\BaseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\BaseQuery(get_called_class());
    }
}