<?php
/* @var $topics \common\models\Topic */
?>

<!-- Sidebar -->
<div class="navbar navbar-fixed-left sidebar">
    <?php if (!empty($topics)) { ?>
        <ul class="list-group">
            <?php foreach ($topics as $topic) { ?>
                <li class="list-group-item"><?= str_repeat('-', $topic->depth) . $topic->title ?></li>
            <?php } ?>
        </ul>
    <?php } ?>
</div>
<!-- /Sidebar -->
