<?php

use yii\widgets\ListView;

/* @var $dateProvider \yii\data\ActiveDataProvider */
?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_comment_item',
    'emptyText' => '',
    'summary' => false
]);
?>

