<?php

use yii\helpers\Url;

/* @var $model \common\models\News */
?>

<div class="jumbotron text-center">
    <a class="h2" href="<?= Url::to(['site/view', 'slug' => $model->slug]) ?>"><?= $model->anons ?></a><br>
    <?= date('d-m-Y H:i', $model->created_at) ?>
    <p><?= $model->content ?></p>
</div>
<hr>
