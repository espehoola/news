<?php

use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = 'My Yii Application';
?>
    <?php Pjax::begin(); ?>

    <!-- Page Content -->
    <div id="content" class="container">
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemView' => '_list_item',
            'layout' => '<div class="col-md-4 col-md-offset-8">' . "{sorter}</div>\n{items}\n{pager}"
        ]);
        ?>
    </div>

    <?php Pjax::end() ?>
