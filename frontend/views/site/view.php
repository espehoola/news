<?php

/* @var $model \common\models\News */
/* @var $newComment \common\models\Comment */
/* @var $dataProvider \yii\data\ActiveDataProvider */
?>

<h1><?= $model->title ?></h1>

<?= date('d-m-Y H:i', $model->created_at) ?>

<p><?= $model->content ?></p>

<?= $this->render('_comments', ['dataProvider' => $dataProvider]) ?>

<?= $this->render('_comment_form', ['newComment' => $newComment]) ?>
