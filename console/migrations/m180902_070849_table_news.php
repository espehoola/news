<?php

use yii\db\Migration;

/**
 * Class m180902_070849_table_news
 */
class m180902_070849_table_news extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%news}}', [
            'id' => $this->primaryKey(),
            'topic_id' => $this->integer(),
            'title' => $this->string(),
            'slug' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'anons' => $this->string(),
            'content' => $this->text(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

    $this->addForeignKey('fk_news_topic', 'news', 'topic_id', 'topic', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180902_070849_table_news cannot be reverted.\n";

        return false;
    }
}
