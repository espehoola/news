<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'status' => $this->smallInteger()->notNull()->defaultValue(0),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ], $tableOptions);

        $columns = [
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status'
        ];

        $rows = [
            [
                'admin',
                'czdMUQhdViILVNg3CosOqKRK15wxml3n',
                '$2y$13$ihNsPZ2ItJcVqSkqy2Z/Q.qk2A74O04g2gML/zSPk/8ZyWs9qyGOC',
                'admin@admin.local',
                \common\models\User::STATUS_ACTIVE
            ]
        ];

        $this->batchInsert('user', $columns, $rows);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
