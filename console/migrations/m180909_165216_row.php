<?php

use yii\db\Migration;

/**
 * Class m180909_165216_row
 */
class m180909_165216_row extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $columns = [
            'username',
            'auth_key',
            'password_hash',
            'email',
            'status'
        ];

        $rows = [
            [
                'admin',
                'czdMUQhdViILVNg3CosOqKRK15wxml3n',
                '$2y$13$ihNsPZ2ItJcVqSkqy2Z/Q.qk2A74O04g2gML/zSPk/8ZyWs9qyGOC',
                'admin@admin.local',
                \common\models\User::STATUS_ACTIVE
            ]
        ];

        $this->batchInsert('user', $columns, $rows);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180909_165216_row cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180909_165216_row cannot be reverted.\n";

        return false;
    }
    */
}
