<?php

use yii\db\Migration;

/**
 * Class m180902_072748_table_comment
 */
class m180902_072748_table_comment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'news_id' => $this->integer(),
            'user_id' => $this->integer(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_at' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addForeignKey('fk_comment_user', 'comment', 'user_id', 'user', 'id');
        $this->addForeignKey('fk_comment_news', 'comment', 'news_id', 'news', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180902_072748_table_comment cannot be reverted.\n";

        return false;
    }
}
